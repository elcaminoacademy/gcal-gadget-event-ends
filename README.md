# README #

This Google Calendar gadget tells you how much time is left in your current event(s). It is a modification of the "[Next meeting](http://www.google.com/ig/modules/calendar/next.xml)" Google Lab.

Add this gadget to your calendar: https://www.google.com/calendar/render?gadgeturl=https://bitbucket.org/elcaminoacademy/gcal-gadget-event-ends/raw/master/event_ends.xml